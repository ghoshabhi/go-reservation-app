package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Room represents
type Room struct {
	ID        string    `json:"roomId"`
	Name      string    `json:"roomName"`
	Type      string    `json:"roomType"`
	CreatedAt time.Time `json:"createdAt"`
	CreatedBy int       `json:"createdBy"`
}

// ValidateInsert instantiates a Room type
func (r *Room) ValidateInsert(room *Room, createdBy int) {
	room.ID = uuid.NewV4().String()
	room.CreatedAt = time.Now()
	room.CreatedBy = createdBy
}

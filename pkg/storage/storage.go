package storage

import "goreservationapp/pkg/storage/models"

// Storage interface defines the methods available to interact with the storage system
type Storage interface {
	AddNewRoom(newRoom *models.Room) error
	GetAllRooms() ([]*models.Room, error)
}

// storage implements the Storage interface
type storage struct {
	rooms []*models.Room
}

// NewStorage configures new storage
func NewStorage() Storage {
	// { "rooms": [ {...}, {...} ] }
	var rooms = make([]*models.Room, 0)

	return &storage{
		rooms: rooms,
	}
}

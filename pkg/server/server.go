package server

import (
	"fmt"
	"goreservationapp/pkg/storage"
	"goreservationapp/pkg/storage/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Server defines the API interface
type Server interface {
	getAllRooms(c *gin.Context)
	addNewRoom(c *gin.Context)
	Start()
}

// server implements the Server interface
type server struct {
	db     storage.Storage
	engine *gin.Engine
}

// NewServer instantiates a new instance of server
func NewServer() Server {
	r := gin.Default()

	s := server{
		db:     storage.NewStorage(),
		engine: r,
	}

	apiMW := r.Group("/api")

	apiMW.GET("/rooms", s.getAllRooms)
	apiMW.POST("/rooms", s.addNewRoom)

	return &s
}

func (s *server) Start() {
	s.engine.Run()
}

func (s *server) getAllRooms(c *gin.Context) {
	var rooms []*models.Room
	var err error

	rooms, err = s.db.GetAllRooms()
	if err != nil {
		fmt.Println("errored")
	}
	c.JSON(http.StatusOK, gin.H{
		"rooms": rooms,
	})
}

func (s *server) addNewRoom(c *gin.Context) {
	var newRoom *models.Room
	var err error

	err = c.ShouldBindJSON(&newRoom)
	if err != nil {
		fmt.Printf("Error binding JSON %v", err)
	}

	newRoom.ValidateInsert(newRoom, 111)

	err = s.db.AddNewRoom(newRoom)
	if err != nil {
		fmt.Printf("Error adding new room: %v", err)
	}

	c.Status(http.StatusNoContent)
}
